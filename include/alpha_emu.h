#ifndef ALPHA_EMU_H
#define ALPHA_EMU_H

#include <stdint.h>
#include <memory>


enum vehicle_mode{
    UNKNOWN = 0,
    // CRUISE = 1,
    DRIVE   = 2, 
    REVERSE = 3, 
    // RADIO = 4,
    MANUAL  = 5
};  


class AlphaEmuSerial;


class AlphaEmu {
public:
    AlphaEmu() = delete;

    AlphaEmu(std::string serial_port);

public:
    // Data from serial
    int16_t throttle_        = 0;  // [-1000:1000]
    int16_t steering_torque_ = 0;  // [-1000:1000]

    vehicle_mode  mode_serial_    = MANUAL; 
    bool hand_brake_state_serial_ = false;
    bool emer_brake_state_serial_ = false;
    bool led_state_serial_        = false;

public:
    // Data from plugin
    int16_t steering_angle_       = 0;  // [deg]
    int16_t steering_velocity_    = 0;  // [deg / sec]
    uint16_t vehicle_speed_       = 0;  // 100 * [m / sec]
    int16_t vehicle_acceleration_ = 0;  // 100 * [m / sec^2]  

    vehicle_mode  mode_plugin_    = UNKNOWN; 
    bool hand_brake_state_plugin_ = false;
    bool emer_brake_state_plugin_ = false;
    bool led_state_plugin_        = false;

private:
    // Private data pointer
    std::shared_ptr<AlphaEmuSerial> impl_;
};

#endif //ALPHA_EMU_H
