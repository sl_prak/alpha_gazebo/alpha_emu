#include "alpha_emu.h"

#include "alpha_emu_msgs/msg/alpha_vehicle_state.hpp"
#include "alpha_emu_msgs/msg/alpha_drive.hpp"

#include "rclcpp/rclcpp.hpp"

#include <chrono>
#include <memory>


std::string NODE_NAME         = "AlphaEmuNode";
std::string INPUT_TOPIC_NAME  = "vehicle_state";
std::string OUTPUT_TOPIC_NAME = "cmd_alpha";

auto PUBLISH_FREQ = std::chrono::duration<double, std::ratio<1,70>>(1);  // 70 Hz


class AlphaEmuNode : public rclcpp::Node{
public:
    // Constructor
    AlphaEmuNode();

private:
    // Read AlphaEmu fields and publish to ros topic
    void timer_drive_cb();

    // Read plugin topic and set AlphaEmu fields
    void sub_cb(const alpha_emu_msgs::msg::AlphaVehicleState::SharedPtr msg) const;

private:
    // Alpha drive data publisher
    rclcpp::Publisher<alpha_emu_msgs::msg::AlphaDrive>::SharedPtr alpha_drive_pub_;

    // Subscriber to plugin drive data
    rclcpp::Subscription<alpha_emu_msgs::msg::AlphaVehicleState>::SharedPtr plugin_sub_;

    // Ros timer for publisher callback
    rclcpp::TimerBase::SharedPtr timer_;

    // Object that works with serial
    std::shared_ptr<AlphaEmu> alpha_emu_;
};


AlphaEmuNode::AlphaEmuNode(): 
        Node(NODE_NAME.c_str()), 
        alpha_emu_(nullptr)
{
    this->declare_parameter("serial_port", "/dev/ALPHA_TEST_VEHICLE_SIDE");
    std::string serial_port = this->get_parameter("serial_port").as_string();

    alpha_emu_ = std::make_shared<AlphaEmu>(serial_port);


    alpha_drive_pub_ = this->create_publisher<alpha_emu_msgs::msg::AlphaDrive>(
            OUTPUT_TOPIC_NAME.c_str(), 10);
    RCLCPP_INFO(this->get_logger(), "Publishing Alpha drive data to [%s]", alpha_drive_pub_->get_topic_name()); 

    plugin_sub_      = this->create_subscription<alpha_emu_msgs::msg::AlphaVehicleState>(
            INPUT_TOPIC_NAME.c_str(), 10, std::bind(&AlphaEmuNode::sub_cb, this, std::placeholders::_1));
    RCLCPP_INFO(this->get_logger(), "Subscribed to [%s]", plugin_sub_->get_topic_name()); 

    timer_ = this->create_wall_timer(
            PUBLISH_FREQ, std::bind(&AlphaEmuNode::timer_drive_cb, this));  
}


void AlphaEmuNode::timer_drive_cb(){   
    auto msg = alpha_emu_msgs::msg::AlphaDrive();

    msg.vehicle_throttle      = alpha_emu_->throttle_;
    msg.steering_torque       = alpha_emu_->steering_torque_;

    msg.mode                  = static_cast<int16_t>(alpha_emu_->mode_serial_);
    msg.hand_brake_state      = alpha_emu_->hand_brake_state_serial_;
    msg.emergency_brake_state = alpha_emu_->emer_brake_state_serial_;
    msg.led_state             = alpha_emu_->led_state_serial_;


    alpha_drive_pub_->publish(msg);
}


void AlphaEmuNode::sub_cb(const alpha_emu_msgs::msg::AlphaVehicleState::SharedPtr msg) const{ 
    alpha_emu_->steering_angle_       = msg->steering_angle;
    alpha_emu_->steering_velocity_    = msg->steering_velocity;
    alpha_emu_->vehicle_speed_        = static_cast<uint16_t>(msg->vehicle_speed * 100);
    alpha_emu_->vehicle_acceleration_ = msg->vehicle_acceleration * 100; 

    alpha_emu_->mode_plugin_ = static_cast<vehicle_mode>(msg->mode);
    alpha_emu_->hand_brake_state_plugin_ = msg->hand_brake_state;
    alpha_emu_->emer_brake_state_plugin_ = msg->emergency_brake_state;
    alpha_emu_->led_state_plugin_ = msg->led_state;
}


int main(int argc, char * argv[]) {
    rclcpp::init(argc, argv);

    auto node = std::make_shared<AlphaEmuNode>();
    rclcpp::spin(node);

    rclcpp::shutdown();
    return 0;
}